/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesAuxiliares;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

/**
 *
 * @author Usuario
 */
public class GeneradorPDF {

    private Font fuenteBold, fuenteNormal, fuenteItalic, fuenteBoldItalic;

    public GeneradorPDF() {
        fuenteBold = new Font(Font.FontFamily.COURIER, 18, Font.BOLD);
        fuenteNormal = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL);
        fuenteItalic = new Font(Font.FontFamily.COURIER, 10, Font.ITALIC);
        fuenteBoldItalic = new Font(Font.FontFamily.COURIER, 14, Font.BOLDITALIC);
    }

    

    public void generarPDF(
            String title,
            String subTitle,
            String info,
            String footer,
            String imagePath,
            String salida) {
        try {
            Document d;
            d = new Document(PageSize.LETTER, 25, 25, 10, 10);
            PdfWriter.getInstance(d, new FileOutputStream(salida));
            d.open();
            d.add(getTitle(title));
            Image img;
            img = Image.getInstance(imagePath);
            img.scaleAbsolute(50, 50);
            img.setAlignment(Element.ALIGN_LEFT);
            d.add(img);
            d.add(getWhiteSpace());

            d.add(getWhiteSpace());
            d.add(getSubtitle(subTitle));
            d.add(getInfo(info));
            d.add(getFooter(footer));
            d.close();
        } catch (Exception e) {
        }
    }
    
        public void generarPDF(
            String title,
            String subTitle,
            String info,
            String subtitle,
            String info2,
            String footer,
            String imagePath,
            String salida) {
        try {
            Document d;
            d = new Document(PageSize.LETTER, 25, 25, 10, 10);
            PdfWriter.getInstance(d, new FileOutputStream(salida));
            d.open();
            d.add(getTitle(title));
            Image img;
            img = Image.getInstance(imagePath);
            img.scaleAbsolute(50, 50);
            img.setAlignment(Element.ALIGN_LEFT);
            d.add(img);
            d.add(getWhiteSpace());
            d.add(getWhiteSpace());
            d.add(getSubtitle(subTitle));
            d.add(getInfo(info));
            d.add(getSubtitle(subtitle));
            d.add(getInfo(info2));
            d.add(getFooter(footer));
            d.close();
        } catch (Exception e) {
        }
    }

    private Paragraph getTitle(String texto) {
        Paragraph p;
        p = new Paragraph();
        Chunk c;
        c = new Chunk();
        p.setAlignment(Element.ALIGN_CENTER);
        c.append(texto);
        c.setFont(fuenteBold);
        p.add(c);
        return p;
    }

    private Paragraph getSubtitle(String texto) {
        Paragraph p;
        p = new Paragraph();
        Chunk c;
        c = new Chunk();
        p.setAlignment(Element.ALIGN_LEFT);
        c.append(texto);
        c.setFont(fuenteBoldItalic);
        p.add(c);
        return p;
    }

    private Paragraph getInfo(String texto) {
        Paragraph p;
        p = new Paragraph();
        Chunk c;
        c = new Chunk();
        p.setAlignment(Element.ALIGN_LEFT);
        c.append(texto);
        c.setFont(fuenteNormal);
        p.add(c);
        return p;
    }

    private Paragraph getFooter(String texto) {
        Paragraph p;
        p = new Paragraph();
        Chunk c;
        c = new Chunk();
        p.setAlignment(Element.ALIGN_RIGHT);
        c.append(texto);
        c.setFont(fuenteItalic);
        p.add(c);
        return p;
    }

    private Paragraph getWhiteSpace() {
        Paragraph p;
        p = new Paragraph();
        Chunk c;
        c = new Chunk();
        p.setAlignment(Element.ALIGN_RIGHT);
        c.append(".");
        c.setFont(fuenteItalic);
        p.add(c);
        return p;
    }
}
