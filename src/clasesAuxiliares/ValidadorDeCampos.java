/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesAuxiliares;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 *
 * @author Usuario
 */
public class ValidadorDeCampos {

    public void u() {

    }

    public static void validarSoloLetras(JTextField campo) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c;
                c = e.getKeyChar();
                if (Character.isDigit(c)) {
                    e.consume();
                }
            }
        });
    }

    public static void validarSoloNumeros(JTextField campo) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c;
                c = e.getKeyChar();
                if (Character.isDigit(c) || c == '.') {
                } else {
                    e.consume();
                }
            }
        });
    }

    public static void limitarCaracteres(JTextField campo, int cantidad) {
        campo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                int cant;
                cant = campo.getText().length();
                if (cant >= cantidad) {
                    e.consume();
                }
            }
        });
    }
}
