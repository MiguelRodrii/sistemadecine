package clasesPrincipales;

import clasesSecundarias.Fecha;
import clasesSecundarias.Hora;
import java.io.Serializable;

/**
 * *********************************************************************
 * Module: Boleto.java Author: lenin Purpose: Defines the Class Boleto
 * *********************************************************************
 */
/**
 * @pdOid f4cf4264-8ada-4a4d-844b-572faf9185ec
 */
public class Boleto implements Serializable {

    /**
     * @pdOid 0f855093-9e61-4c80-bc05-47e2fe928322
     */
    private Pelicula peliculaASerVista;
    /**
     * @pdOid d20edc08-bc42-4e3b-a50a-133e2cfc8642
     */
    private String asiento;
    /**
     * @pdOid f98e7b81-1797-4f62-94a3-6ee6f662d366
     */
    private String tipoDeBoleto;
    /**
     * @pdOid 405dfe12-354e-436b-9d53-df33eb3e27dc
     */
    private double costo;
    /**
     * @pdOid 210c3f4e-ee7d-4fe5-8e9c-aa1afe83a912
     */
    private int idDeBoleto;
    /**
     * @pdOid bdb980e4-d2e1-44bd-a454-6275223c40e0
     */
    private Hora horaInicio;
    /**
     * @pdOid 6a03c283-85a2-481a-be90-f35eb9c9cbe0
     */
    private String diaDeFuncion;

    private Funcion funcionASerVista;

    /**
     * @return * @pdOid d1be1f82-aa5e-45b8-aa4e-164323af088b
     */
    public Pelicula getPeliculaASerVista() {
        return peliculaASerVista;
    }

    /**
     * @param newPeliculaASerVista
     * @pdOid 96255eba-ea0a-4902-8ffc-7309a68eaaf6
     */
    public void setPeliculaASerVista(Pelicula newPeliculaASerVista) {
        peliculaASerVista = newPeliculaASerVista;
    }

    /**
     * @return * @pdOid 18caea22-056d-4661-8101-b8eb1b14d7f6
     */
    public String getAsiento() {
        return asiento;
    }

    /**
     * @param newAsiento
     * @pdOid f153438d-e129-4b65-9abb-924a57fb2512
     */
    public void setAsiento(String newAsiento) {
        asiento = newAsiento;
    }

    /**
     * @return * @pdOid c3b88dc6-70ae-4da7-b9b8-335d4eccc623
     */
    public String getTipoDeBoleto() {
        return tipoDeBoleto;
    }

    /**
     * @param newTipoDeBoleto
     * @pdOid 82880e2d-3645-4754-9546-6f78d6181121
     */
    public void setTipoDeBoleto(String newTipoDeBoleto) {
        tipoDeBoleto = newTipoDeBoleto;
    }

    /**
     * @return * @pdOid c2c901e5-f208-48c3-99fe-9e2063d89e1e
     */
    public double getCosto() {
        return costo;
    }

    /**
     * @param newCosto
     * @pdOid c6d6b161-f81f-420a-a141-7c735a081934
     */
    public void setCosto(double newCosto) {
        costo = newCosto;
    }

    /**
     * @return * @pdOid aa1cd4ed-ad24-4b84-a3eb-cd04b57ec5aa
     */
    /**
     * @param newIDBoleto
     * @pdOid 2985fd69-1a39-4c8a-8ee6-4bc942c895f8
     */
    /**
     * @return * @pdOid fbb2c84e-50b2-401e-a86d-bae2637899f8
     */
    /**
     * @return * @pdOid e642cf1b-24e6-4089-a332-580eb3cac297
     */
    public Hora getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param newHoraInicio
     * @pdOid fa813748-13d7-43af-9d59-77ea8ee23547
     */
    public void setHoraInicio(Hora newHoraInicio) {
        horaInicio = newHoraInicio;
    }

    /**
     * @return * @pdOid 9ff39905-268b-46fa-83cf-ce1790056c7f
     */
    public String getDiaDeFuncion() {
        return diaDeFuncion;
    }

    /**
     * @param newDiaDeFuncion
     * @pdOid 5ca5f2d2-3a93-472e-9207-01d66633c3e5
     */
    public void setDiaDeFuncion(String newDiaDeFuncion) {
        diaDeFuncion = newDiaDeFuncion;
    }

    /**
     * @return * @pdOid c5e1918b-3826-4e24-ab4d-e3701b18da7c
     */
    public int getIdDeBoleto() {
        return idDeBoleto;
    }

    /**
     * @param newIdBoleto
     * @pdOid f88af58e-d79a-425c-8a8d-ba38f1e3f727
     */
    public void setIdDeBoleto(int newIdBoleto) {
        idDeBoleto = newIdBoleto;
    }

    public Funcion getFuncionASerVista() {
        return funcionASerVista;
    }

    public void setFuncionASerVista(Funcion funcionASerVista) {
        this.funcionASerVista = funcionASerVista;
    }

    // Constructor
    public Boleto(Pelicula peliculaASerVista, String asiento, String tipoDeBoleto, double costo, int idDeBoleto, Hora horaInicio, String diaDeFuncion, Funcion funcionASerVista) {
        this.peliculaASerVista = peliculaASerVista;
        this.asiento = asiento;
        this.tipoDeBoleto = tipoDeBoleto;
        this.costo = costo;
        this.idDeBoleto = idDeBoleto;
        this.horaInicio = horaInicio;
        this.diaDeFuncion = diaDeFuncion;
        this.funcionASerVista = funcionASerVista;
    }

    public String representarAsiento() {
        String[] aux;
        aux = new String[]{"A", "B", "C", "D", "E", "F"};
        String[] aux2;
        aux2 = asiento.split(";");
        return aux[Integer.parseInt(aux2[0])] + (Integer.parseInt(aux2[1]) + 1) + "";
    }

    public String representar() {
        return "\n"
                + "--------------------------------------------" + "\n"
                + "Número de boleto: " + idDeBoleto + "\n"
                + "Pelicula: " + peliculaASerVista.getNombre() + "\n"
                + "Tipo de boleto: " + tipoDeBoleto + "\n"
                + "Día: " + diaDeFuncion + "\n"
                + "Hora de inicio: " + horaInicio.toString() + "\n"
                + "Asiento: " + representarAsiento() + "\n"
                + "Valor: " + costo + "\n"
                + "--------------------------------------------" + "\n";
    }
}
