package clasesPrincipales;

/**
 * *********************************************************************
 * Module: Cine.java Author: Usuario Purpose: Defines the Class Cine
 * *********************************************************************
 */
import clasesSecundarias.Fecha;
import clasesSecundarias.Hora;
import exceptions.excepcionCedulaInvalida;
import java.io.Serializable;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

/**
 * @pdOid b567c753-608c-47b5-8511-8f93d1e96a9c
 */
public class Cine implements Serializable {

    /**
     * @pdOid f54cd98c-f60f-4999-8faf-2b4c6dff8ce0
     */
    private String nombre;
    /**
     * @pdOid a26ec795-b83e-46c9-bb74-08e926b592de
     */
    private String asociacion;
    /**
     * @pdOid c9eeb990-f60b-46c9-8cb2-6887f468b758
     */
    private HashMap<Integer, Pelicula> catalogoDePeliculas;
    /**
     * @pdOid 4364a250-8903-400e-bab4-681d156e78be
     */
    /**
     * @pdOid d37372fd-7adb-436e-9e1e-bc1783b32d7d
     */
    private HashMap<Integer, Factura> registroFacturas;
    /**
     * @pdOid 43d070a4-4e77-494d-8d82-e6cd31ff3faf
     */
    private HashMap<Integer, Funcion> catalogoDeFunciones;

    /**
     * @param newNombre
     * @pdOid 0c177e34-dcef-4631-b2a8-076f0d89d546
     */
    public void setNombre(String newNombre) {
        nombre = newNombre;
    }

    /**
     * @return * @pdOid 0b8e60e7-9b0f-459e-b68f-5b3d9c13fff8
     */
    public String getAsociacion() {
        return asociacion;
    }

    /**
     * @param newAsociacion
     * @pdOid 365c72bd-b014-4eab-8444-89b3aa48cc54
     */
    public void setAsociacion(String newAsociacion) {
        asociacion = newAsociacion;
    }

    /**
     * @return * @pdOid 60f7b157-a58f-4ca3-8721-d91aadc311d4
     */
    public HashMap<Integer, Pelicula> getCatalogoDePeliculas() {
        return catalogoDePeliculas;
    }

    /**
     * @param newCatalogoDePeliculas
     * @pdOid 4772b3af-d19f-4cb9-bed3-4bb51e781a0c
     */
    public void setCatalogoDePeliculas(HashMap<Integer, Pelicula> newCatalogoDePeliculas) {
        catalogoDePeliculas = newCatalogoDePeliculas;
    }

    /**
     * @return * @pdOid 92fb3b1d-3c0c-479d-9e0b-a73f80e7fd5a
     */
    /**
     * @return * @pdOid bacd5821-8ac0-4b7b-b885-b01c5781bf88
     */
    public HashMap<Integer, Factura> getRegistroFacturas() {
        return registroFacturas;
    }

    /**
     * @param newRegistroFacturas
     * @pdOid a04f38d1-28c5-4fc3-a23d-a8420efb88c9
     */
    public void setRegistroFacturas(HashMap<Integer, Factura> newRegistroFacturas) {
        registroFacturas = newRegistroFacturas;
    }

    /**
     * @return * @pdOid d9d415e7-a0b4-48d0-ad90-df645b7a4423
     */
    public HashMap<Integer, Funcion> getCatalogoDeFunciones() {
        return catalogoDeFunciones;
    }

    /**
     * @param newCatalogoDeFunciones
     * @pdOid 20d866cd-0cb7-43d7-a965-24deb64e2629
     */
    public void setCatalogoDeFunciones(HashMap<Integer, Funcion> newCatalogoDeFunciones) {
        catalogoDeFunciones = newCatalogoDeFunciones;
    }

    public String getNombre() {
        return nombre;
    }

    // Constructor 
    public Cine(String nombre, String asociacion) {
        this.nombre = nombre;
        this.asociacion = asociacion;
        this.catalogoDePeliculas = new HashMap<>();
        this.registroFacturas = new HashMap<>();
        this.catalogoDeFunciones = new HashMap<>();
    }

    // Other methods
    public HashMap<Integer, Funcion> filtrarFuncionesPorCategoria(HashMap<Integer, Funcion> funcionesDisponibles, String categoria) {
        try {
            HashMap<Integer, Funcion> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getPeliculaAserProyectada().getCategoriaDePelicula().equals(categoria)) {
                    respuesta.put(i.getIdDeFuncion(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }

    }

    public HashMap<Integer, Funcion> filtrarFuncionesPorDia(HashMap<Integer, Funcion> funcionesDisponibles, String dia) {
        try {
            HashMap<Integer, Funcion> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getDia().equals(dia)) {
                    respuesta.put(i.getIdDeFuncion(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }

    }

    public HashMap<Integer, Funcion> filtrarFuncionesPorDimension(HashMap<Integer, Funcion> funcionesDisponibles, String dimension) {
        try {
            HashMap<Integer, Funcion> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getPeliculaAserProyectada().getDimension().equals(dimension)) {
                    respuesta.put(i.getIdDeFuncion(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }

    }

    public HashMap<Integer, Funcion> filtrarFuncionesPorSeccionDelDia(HashMap<Integer, Funcion> funcionesDisponibles, String seccion) {
        try {
            HashMap<Integer, Funcion> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getHoraInicio().comprobarSeccionDeDia().equals(seccion)) {
                    respuesta.put(i.getIdDeFuncion(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public HashMap<Integer, Funcion> filtrarFuncionesPorHora(HashMap<Integer, Funcion> funcionesDisponibles, Hora hor) {
        try {
            HashMap<Integer, Funcion> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (Hora.comparar(i.getHoraInicio(), hor) == 0) {
                    respuesta.put(i.getIdDeFuncion(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public Pelicula registrarPelicula(String nombre, String sinopsis, Hora duracion, String clasificacion, String categoriaDePelicula, String dimension, String rutaDePortada) {
        try {
            Pelicula aux;
            aux = new Pelicula(nombre, sinopsis, duracion, clasificacion, categoriaDePelicula, dimension, this.catalogoDePeliculas.size(), rutaDePortada);
            boolean auxb;
            auxb = true;
            for (Pelicula j : catalogoDePeliculas.values()) {
                if (j.getNombre().equals(nombre) && j.getDimension().equals(dimension)) {
                    auxb = false;
                    break;
                }
            }
            if (auxb) {
                catalogoDePeliculas.put(aux.getIdDePelicula(), aux);
                return aux;
            } else {
                JOptionPane.showMessageDialog(null, "No es posible registrar esa pelicula puesto que ya ha sido registrada previamente.");
                return null;
            }
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public Funcion registrarFuncion(Pelicula peliculaAserProyectada, Hora horaInicio, Double costoGeneral, Double porcentajeAdulto, Double porcentajeNino, Double porcentajeDiscapacitado, Double porcentajeAdultoMayor, String dia) {
        try {
            boolean registroPrevioDeDia, registroPrevioDeHora;
            registroPrevioDeDia = false;
            registroPrevioDeHora = false;

            Funcion aux;
            aux = new Funcion(
                    peliculaAserProyectada,
                    horaInicio,
                    costoGeneral,
                    porcentajeAdulto,
                    porcentajeNino,
                    porcentajeDiscapacitado,
                    porcentajeAdultoMayor,
                    dia,
                    this.catalogoDeFunciones.size()
            );

            // Comprobacion de si hay una función de esa película creada el mismo día
            for (Funcion j : catalogoDeFunciones.values()) {
                if (j.getDia().equals(dia)) {
                    registroPrevioDeDia = true;
                    break;
                }
            }
            if (registroPrevioDeDia) {
                for (Funcion j : catalogoDeFunciones.values()) {
                    for (int i = aux.getHoraInicio().transformarAMinutos(); i < aux.getHoraFin().transformarAMinutos(); i++) {
                        if (i >= j.getHoraInicio().transformarAMinutos() && i <= j.getHoraFin().transformarAMinutos()) {
                            registroPrevioDeHora = true;
                            break;
                        }
                    }
                }
                if (registroPrevioDeHora) {
                    JOptionPane.showMessageDialog(null, "Ya existe una función registrada en el mismo día y la misma hora");
                    return null;
                } else {
                    this.catalogoDeFunciones.put(aux.getIdDeFuncion(), aux);
                    return aux;
                }
            } else { // Esto significa que no hay una funcion registrada en ese día
                this.catalogoDeFunciones.put(aux.getIdDeFuncion(), aux);
                return aux;
            }
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public HashMap<Integer, Pelicula> filtrarPeliculasPorCategoria(HashMap<Integer, Pelicula> funcionesDisponibles, String categoria) {
        try {
            HashMap<Integer, Pelicula> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getCategoriaDePelicula().equals(categoria)) {
                    respuesta.put(i.getIdDePelicula(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public HashMap<Integer, Pelicula> filtrarPeliculasPorDimension(HashMap<Integer, Pelicula> funcionesDisponibles, String dimension) {
        try {
            HashMap<Integer, Pelicula> respuesta;
            respuesta = new HashMap<>();
            funcionesDisponibles.values().forEach((i) -> {
                if (i.getDimension().equals(dimension)) {
                    respuesta.put(i.getIdDePelicula(), i);
                }
            });
            return respuesta;
        } catch (Exception e) {
            System.out.println("" + e);
            return null;
        }
    }

    public String[] definirAsientosSeleccionados(JToggleButton[][] botones, int nDeAsientos) {
        String[] aux;
        aux = new String[nDeAsientos];
        int cont;
        cont = 0;
        for (int i = 0; i < botones.length; i++) {
            for (int j = 0; j < botones[i].length; j++) {
                if (botones[i][j].isSelected()) {
                    aux[cont] = "" + i + ";" + j;
                    System.out.println("" + i + j);
                    cont++;
                }
            }
        }
        return aux;
    }

    public double[] estimarCostoDeFactura(Funcion funcionSeleccionada, int nBAdulto, int nBAdultoDiscapacitado, int nBAdultoMayor, int nBAdultoMayorDiscapacitado, int nBNiño, int nBNiñoDiscapacitado, String[] puestos) {
        int c;
        c = 0;
        HashMap<Integer, Boleto> aux;
        aux = new HashMap<>();
        for (int i = 0; i < nBAdulto; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Adulto / Adolescente",
                    funcionSeleccionada.getCostoDeBoletoDeAdulto(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        System.out.println("1done");
        for (int i = 0; i < nBAdultoDiscapacitado; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Adulto con discapacidad / Adolescente con discapacidad",
                    funcionSeleccionada.getCostoDeBoletoDeAdultoDiscapacitado(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        System.out.println("2done");
        for (int i = 0; i < nBAdultoMayor; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Adulto mayor",
                    funcionSeleccionada.getCostoDeBoletoDeAdultoMayor(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        System.out.println("3done");
        for (int i = 0; i < nBAdultoMayorDiscapacitado; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Adulto mayor con discapacidad",
                    funcionSeleccionada.getCostoDeBoletoDeAdultoMayorDiscapacitado(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        System.out.println("4done");
        for (int i = 0; i < nBNiño; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Niño",
                    funcionSeleccionada.getCostoDeBoletoDeNino(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        System.out.println("5done");
        for (int i = 0; i < nBNiñoDiscapacitado; i++) {
            aux.put(aux.size() + 1, new Boleto(
                    funcionSeleccionada.getPeliculaAserProyectada(),
                    puestos[c],
                    "Niño con discapacidad",
                    funcionSeleccionada.getCostoDeBoletoDeNinoDiscapacitado(),
                    aux.size() + 1,
                    funcionSeleccionada.getHoraInicio(),
                    funcionSeleccionada.getDia(),
                    funcionSeleccionada));
            c++;
        }
        double[] resp;
        resp = new double[3];
        for (Boleto j : aux.values()) {
            resp[0] = resp[0] + j.getCosto();
        }
        resp[1] = resp[0] * 0.12;
        resp[2] = resp[0] - resp[1];
        return resp;

    }

    public Factura facturar(Funcion funcionSeleccionada, int nBAdulto, int nBAdultoDiscapacitado, int nBAdultoMayor, int nBAdultoMayorDiscapacitado, int nBNiño, int nBNiñoDiscapacitado, String[] puestos, String nombre, String cedula, String direccion, String telefono) {
        try {
            int c;
            c = 0;
            HashMap<Integer, Boleto> aux;
            aux = new HashMap<>();
            for (int i = 0; i < nBAdulto; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Adulto / Adolescente",
                        funcionSeleccionada.getCostoDeBoletoDeAdulto(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("1done");
            for (int i = 0; i < nBAdultoDiscapacitado; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Adulto con discapacidad / Adolescente con discapacidad",
                        funcionSeleccionada.getCostoDeBoletoDeAdultoDiscapacitado(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("2done");
            for (int i = 0; i < nBAdultoMayor; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Adulto mayor",
                        funcionSeleccionada.getCostoDeBoletoDeAdultoMayor(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("3done");
            for (int i = 0; i < nBAdultoMayorDiscapacitado; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Adulto mayor con discapacidad",
                        funcionSeleccionada.getCostoDeBoletoDeAdultoMayorDiscapacitado(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("4done");
            for (int i = 0; i < nBNiño; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Niño",
                        funcionSeleccionada.getCostoDeBoletoDeNino(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("5done");
            for (int i = 0; i < nBNiñoDiscapacitado; i++) {
                aux.put(aux.size() + 1, new Boleto(
                        funcionSeleccionada.getPeliculaAserProyectada(),
                        puestos[c],
                        "Niño con discapacidad",
                        funcionSeleccionada.getCostoDeBoletoDeNinoDiscapacitado(),
                        aux.size() + 1,
                        funcionSeleccionada.getHoraInicio(),
                        funcionSeleccionada.getDia(),
                        funcionSeleccionada));
                c++;
            }
            System.out.println("6done");

            Factura auxFactura;
            auxFactura = new Factura(cedula, nombre, direccion, telefono, registroFacturas.size()+1, aux);
            System.out.println("Factura creada");

            registroFacturas.put(auxFactura.getNumeroDeFactura(), auxFactura);
            System.out.println("Factura registrada");
            return auxFactura;
        } catch (excepcionCedulaInvalida e) {
            JOptionPane.showMessageDialog(null, "" + e);
            return null;
        }
    }
}
