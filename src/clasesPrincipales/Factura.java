package clasesPrincipales;

/**
 * *********************************************************************
 * Module: Factura.java Author: Usuario Purpose: Defines the Class Factura
 * *********************************************************************
 */
import clasesAuxiliares.GeneradorPDF;
import clasesSecundarias.Fecha;
import clasesSecundarias.Hora;
import java.io.Serializable;
import java.util.*;
import javax.swing.table.DefaultTableModel;
import exceptions.excepcionCedulaInvalida;

/**
 * @pdOid 0aec50c7-7693-4a8f-8808-d3633ed680d8
 */
public class Factura implements Serializable {

    /**
     * @pdOid 33d3a90c-d82a-4a59-8aed-bfeba50faf31
     */
    private String cedula;
    /**
     * @pdOid d20a0680-1aa5-460c-8b4d-baa99dc487d4
     */
    private String nombre;
    /**
     * @pdOid fbe59528-69b2-4466-be93-6afeb46e083c
     */
    private Fecha fechaDeEmision;
    /**
     * @pdOid efca09b6-6184-41fd-8b42-f3456c9612b6
     */
    private String direccion;
    /**
     * @pdOid 9a59c8a3-a892-41c7-91d1-05ed8a55c72e
     */
    private String telefono;
    /**
     * @pdOid b5611125-6dd4-45b9-9e5b-e024be55bab9
     */
    private HashMap<Integer, Boleto> boletosVendidos;
    /**
     * @pdOid 24d0d7c8-249e-46bc-b821-dc716d98f4fe
     */
    private Double total;
    /**
     * @pdOid 1c2b5fab-b93e-4e6f-82f6-c22debf60fc9
     */
    private Double subTotal;
    /**
     * @pdOid a24196f2-cfc9-4044-82b7-4822050135a2
     */
    private Double iva;
    /**
     * @pdOid 608ce20d-f4ce-4653-ae9d-2fcaa7395b8f
     */
    private int numeroDeFactura;

    /**
     * @return * @pdOid a04b4bd5-ef44-494f-b1a7-d6063b386857
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param newCedula
     * @pdOid dbb5c1fa-df40-4a78-a63c-b0e485f6a4a3
     */
    public void setCedula(String newCedula) {
        cedula = newCedula;
    }

    /**
     * @return * @pdOid 23515cfd-2c26-47a1-8dc2-ab10e8695bc1
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param newNombre
     * @pdOid e00cdb50-2865-4449-bdaf-2be3713822c9
     */
    public void setNombre(String newNombre) {
        nombre = newNombre;
    }

    /**
     * @return * @pdOid a18f7366-1214-4659-ae09-407eefb807fb
     */
    public Fecha getFechaDeEmision() {
        return fechaDeEmision;
    }

    /**
     * @param newFechaDeEmision
     * @pdOid cf870f3e-ad02-4a7b-b984-0834e0b60959
     */
    public void setFechaDeEmision(Fecha newFechaDeEmision) {
        fechaDeEmision = newFechaDeEmision;
    }

    /**
     * @return * @pdOid dfe7c17d-570e-43a2-95ca-fe3e1098c45c
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param newDireccion
     * @pdOid 2233502c-61d6-45de-9d82-c912ceba6461
     */
    public void setDireccion(String newDireccion) {
        direccion = newDireccion;
    }

    /**
     * @return * @pdOid 8c29bfb0-3d50-4d12-915f-36c27d641c6a
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param newTelefono
     * @pdOid 7c105519-7095-4385-8520-31487f19a078
     */
    public void setTelefono(String newTelefono) {
        telefono = newTelefono;
    }

    public Double getTotal() {
        return total;
    }

    /**
     * @param newTotal
     * @pdOid 79c5855a-378d-4c8f-9a11-1f3325b7b8b3
     */
    public void setTotal(Double newTotal) {
        total = newTotal;
    }

    /**
     * @return * @pdOid 15ec618c-9cf7-4467-ad9d-9e18621d6132
     */
    public Double getSubTotal() {
        return subTotal;
    }

    /**
     * @param newSubTotal
     * @pdOid d51969e4-b56e-47b2-97f6-2146578abd6a
     */
    public void setSubTotal(Double newSubTotal) {
        subTotal = newSubTotal;
    }

    /**
     * @return * @pdOid fde8f16c-0c79-41f3-85dc-d902c4654598
     */
    public Double getIva() {
        return iva;
    }

    /**
     * @param newIva
     * @pdOid c335fab5-9691-4860-82fa-f61464773024
     */
    public void setIva(Double newIva) {
        iva = newIva;
    }

    /**
     * @return * @pdOid 7844d2fa-a639-4051-83d2-5dc701cc4961
     */
    public int getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public HashMap<Integer, Boleto> getBoletosVendidos() {
        return boletosVendidos;
    }

    public void setBoletosVendidos(HashMap<Integer, Boleto> boletosVendidos) {
        this.boletosVendidos = boletosVendidos;
    }

    /**
     * @param newNumeroDeFactura
     * @pdOid 954f5811-a9db-4705-b170-b17fdcb86d5f
     */
    public void setNumeroDeFactura(int newNumeroDeFactura) {
        numeroDeFactura = newNumeroDeFactura;
    }

    // Constructor
    public Factura(String cedula, String nombre, String direccion, String telefono, int numeroDeFactura, HashMap<Integer, Boleto> boletosVendidos) throws excepcionCedulaInvalida {
        if (validarCedula(cedula)) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaDeEmision = new Fecha();
        this.direccion = direccion;
        this.telefono = telefono;
        this.boletosVendidos = boletosVendidos;
        System.out.println("Asignacion de valores iniciales en la factura correctos");
        this.total = 0.0;
        for (Boleto j : boletosVendidos.values()) {
            this.total = this.total + j.getCosto();
        }
        System.out.println("Calculo de precios hecho");
        this.iva = this.total * 0.12;
        this.subTotal = this.total - this.iva;
        this.numeroDeFactura = numeroDeFactura;
        System.out.println("Creacion de factura finalizado");    
        } else {
            throw new excepcionCedulaInvalida();
        }
        
    }

    public void limpiarTabla(DefaultTableModel tabla) {
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }

    }

    public String representarTodosLosBoletos() {
        String aux;
        aux = "";
        System.out.println("impresion previa de boletos");
        for (Boleto j : boletosVendidos.values()) {
            aux = aux + j.representar();
        }
        System.out.println("Impresion de boleatos correcta");
        return aux;
    }

    public void representarEnPDF(String ruta) {
        new GeneradorPDF().generarPDF(
                "Factura de compra y venta de boletos de cine",
                "Datos de la factura",
                "Numero / Id. de factura: " + numeroDeFactura + "\n"
                + "Cédula del cliente: " + cedula + "\n"
                + "Nombre del cliente: " + nombre + "\n"
                + "Teléfono: " + telefono + "\n"
                + "Dirección: " + direccion,
                "Boletos",
                "" + representarTodosLosBoletos(),
                "Comprobante generado el: " + new Fecha().representar(),
                getClass().getResource("/medios/imgFactura.jpg").toString(),
                ruta + ".pdf"
        );
    }

    public static boolean validarCedula(String cedula) {
        boolean cedulaCorrecta = false;
        try {

            if (cedula.length() == 10) // ConstantesApp.LongitudCedula
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
// Coeficientes de validación cédula
// El decimo digito se lo considera dígito verificador
                    int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};
                    int verificador = Integer.parseInt(cedula.substring(9, 10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    } else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            System.out.println("Una excepcion ocurrio en el proceso de validadcion");
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
            System.out.println("La Cédula ingresada es Incorrecta");
        }
        return cedulaCorrecta;
    }

//        private String diaCorrecto() {
//        if (fechaDeAtencion.getDia() <= 9) {
//            return "0" + fechaDeAtencion.getDia();
//        } else {
//            return "" + fechaDeAtencion.getDia();
//        }
//    }
//    
//    private String mesCorrecto() {
//        if (fechaDeAtencion.getMes() <= 9) {
//            return "0" + fechaDeAtencion.getMes();
//        } else {
//            return "" + fechaDeAtencion.getMes();
//        }
//    }
//
//    private String numeroDeFacturaCorrecto() {
//        String r;
//        r = "" + numeroDeFactura;
//        int digitos;
//        digitos = Integer.toString(numeroDeFactura).length();
//        if (digitos == 9) {
//            return "" + numeroDeFactura;
//        } else {
//            int x;
//            x = (digitos - 9) * -1;
//            for (int i = 0; i < x; i++) {
//                r = "0" + r;
//            }
//            return r;
//        }
//    }
//
//    public String invertirCadena(String cadena) {
//        String cadenaInvertida = "";
//        for (int x = cadena.length() - 1; x >= 0; x--) {
//            cadenaInvertida = cadenaInvertida + cadena.charAt(x);
//        }
//        return cadenaInvertida;
//    }
//
//    public int obtenerSumaPorDigitos(String cadena) {
//        int pivote = 2;
//        int longitudCadena = cadena.length();
//        int cantidadTotal = 0;
//        int b = 1;
//        for (int i = 0; i < longitudCadena; i++) {
//            if (pivote == 8) {
//                pivote = 2;
//            }
//            int temporal = Integer.parseInt("" + cadena.substring(i, b));
//            b++;
//            temporal *= pivote;
//            pivote++;
//            cantidadTotal += temporal;
//        }
//        cantidadTotal = 11 - cantidadTotal % 11;
//        return cantidadTotal;
//    }
//
//    public String cadenaDeVerificacion() {
//        String respuesta;
//        respuesta = "";
//        respuesta = respuesta + diaCorrecto() + mesCorrecto() + fechaDeAtencion.getAño() + "01"
//                + doctor.getHospitalDeTrabajo().getCedulaDelPropietario()
//                + "0011000000" + numeroDeFacturaCorrecto() + "000000352";
//        int aux;
//        aux = obtenerSumaPorDigitos(invertirCadena(respuesta));
//        respuesta = respuesta + "" + aux;
//        return respuesta;
//    }
}
