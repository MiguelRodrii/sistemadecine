package clasesPrincipales;

import clasesAuxiliares.GeneradorPDF;
import clasesSecundarias.Fecha;
import clasesSecundarias.Hora;
import exceptions.exceptionCostoDeFuncionInvalido;
import exceptions.exceptionHoraIncorrecta;
import exceptions.exceptionMinutosIncorrectos;
import java.io.Serializable;
import javafx.scene.control.ToggleButton;
import javax.swing.table.DefaultTableModel;

/**
 * *********************************************************************
 * Module: Funcion.java Author: Usuario Purpose: Defines the Class Funcion
 * *********************************************************************
 */
/**
 * @pdOid 902b8c5d-7bf5-4d20-806b-8bee234df514
 */
public class Funcion implements Serializable {

    /**
     * @pdOid 0374372b-be45-49ba-9a95-605c339d7e56
     */
    private Pelicula peliculaAserProyectada;
    /**
     * @pdOid a70b3b7a-060f-4edb-9fe9-93f981890adf
     */
    private Hora horaInicio;
    /**
     * @pdOid 1721274a-b2b4-4f5b-a1a6-5762319db5d5
     */
    private Hora horaFin;
    /**
     * @pdOid 1d491bce-0407-4e87-8ecd-65a5d1cb2638
     */
    private Double costoGeneral;
    /**
     * @pdOid c98a4272-0e12-4cea-a827-49729c43034a
     */
    private Double costoDeBoletoDeAdulto;
    /**
     * @pdOid a076ba72-b584-4185-9d96-db12ecbc655f
     */
    private Double costoDeBoletoDeNino;
    /**
     * @pdOid 6b744afb-4ac0-4d12-8e9d-63c6e4c4e0ce
     */
    private Double costoDeBoletoDeAdultoMayor;
    /**
     * @pdOid c98a4272-0e12-4cea-a827-49729c43034a
     */
    private Double costoDeBoletoDeAdultoDiscapacitado;
    /**
     * @pdOid a076ba72-b584-4185-9d96-db12ecbc655f
     */
    private Double costoDeBoletoDeNinoDiscapacitado;
    /**
     * @pdOid 6b744afb-4ac0-4d12-8e9d-63c6e4c4e0ce
     */
    private Double costoDeBoletoDeAdultoMayorDiscapacitado;
    /**
     * @pdOid 05651789-684c-4c93-9016-51bdd19ac515
     */
    private boolean[][] salaDondeSeProyectara;
    /**
     * @pdOid ff040898-3d7e-49e9-a6ad-e0a372549f26
     */
    private String dia;
    /**
     * @pdOid 9b3d7b7f-9202-42c2-9649-e5ae2c722a77
     */
    private int idDeFuncion;
    private String seccionDelDia;

    private boolean aptitudParaMenores;

    private boolean aptitudParaAdolescentes;

    /**
     * @return * @pdOid b50422b5-557e-4d04-b253-a25a4f708acb
     */
    public Pelicula getPeliculaAserProyectada() {
        return peliculaAserProyectada;
    }

    /**
     * @param newPeliculaAserProyectada
     * @pdOid 207eeef3-7bde-4030-9eef-84324dbae880
     */
    public void setPeliculaAserProyectada(Pelicula newPeliculaAserProyectada) {
        peliculaAserProyectada = newPeliculaAserProyectada;
    }

    /**
     * @return * @pdOid 7a6b099b-009a-477e-9ab6-8ff6ace56afd
     */
    public Hora getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param newHoraInicio
     * @pdOid 73fff953-e5f0-440c-9a1d-11db98aedbd1
     */
    public void setHoraInicio(Hora newHoraInicio) {
        horaInicio = newHoraInicio;
    }

    /**
     * @return * @pdOid 65511364-bbfa-4f5e-8145-3a9382fd8671
     */
    public Hora getHoraFin() {
        return horaFin;
    }

    /**
     * @param newHoraFin
     * @pdOid f0607a81-0ebc-42fe-9dcc-4731835afafc
     */
    public void setHoraFin(Hora newHoraFin) {
        horaFin = newHoraFin;
    }

    /**
     * @return * @pdOid d1714c49-242b-4172-a081-6db2a17426ee
     */
    public Double getCostoGeneral() {
        return costoGeneral;
    }

    /**
     * @param newCostoGeneral
     * @pdOid 7c4dc993-98b6-44eb-94f0-fc95c17c1c4c
     */
    public void setCostoGeneral(Double newCostoGeneral) {
        costoGeneral = newCostoGeneral;
    }

    /**
     * @return * @pdOid cbd53b6e-2934-430f-83fa-9d960881eeed
     */
    /**
     * @return * @pdOid 88fd57c1-a94c-415d-aa10-4f6107b804ec
     */
    public int getIdDeFuncion() {
        return idDeFuncion;
    }

    /**
     * @param newIdDeFuncion
     * @pdOid b68ce72d-9c0b-4e4e-8a26-b0a7ae800bc3
     */
    public void setIdDeFuncion(int newIdDeFuncion) {
        idDeFuncion = newIdDeFuncion;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getSeccionDelDia() {
        return seccionDelDia;
    }

    public void setSeccionDelDia(String seccionDelDia) {
        this.seccionDelDia = seccionDelDia;
    }

    public Double getCostoDeBoletoDeAdulto() {
        return costoDeBoletoDeAdulto;
    }

    public void setCostoDeBoletoDeAdulto(Double costoDeBoletoDeAdulto) {
        this.costoDeBoletoDeAdulto = costoDeBoletoDeAdulto;
    }

    public Double getCostoDeBoletoDeNino() {
        return costoDeBoletoDeNino;
    }

    public void setCostoDeBoletoDeNino(Double costoDeBoletoDeNino) {
        this.costoDeBoletoDeNino = costoDeBoletoDeNino;
    }

    public Double getCostoDeBoletoDeAdultoMayor() {
        return costoDeBoletoDeAdultoMayor;
    }

    public void setCostoDeBoletoDeAdultoMayor(Double costoDeBoletoDeAdultoMayor) {
        this.costoDeBoletoDeAdultoMayor = costoDeBoletoDeAdultoMayor;
    }

    public Double getCostoDeBoletoDeAdultoDiscapacitado() {
        return costoDeBoletoDeAdultoDiscapacitado;
    }

    public void setCostoDeBoletoDeAdultoDiscapacitado(Double costoDeBoletoDeAdultoDiscapacitado) {
        this.costoDeBoletoDeAdultoDiscapacitado = costoDeBoletoDeAdultoDiscapacitado;
    }

    public Double getCostoDeBoletoDeNinoDiscapacitado() {
        return costoDeBoletoDeNinoDiscapacitado;
    }

    public void setCostoDeBoletoDeNinoDiscapacitado(Double costoDeBoletoDeNinoDiscapacitado) {
        this.costoDeBoletoDeNinoDiscapacitado = costoDeBoletoDeNinoDiscapacitado;
    }

    public Double getCostoDeBoletoDeAdultoMayorDiscapacitado() {
        return costoDeBoletoDeAdultoMayorDiscapacitado;
    }

    public void setCostoDeBoletoDeAdultoMayorDiscapacitado(Double costoDeBoletoDeAdultoMayorDiscapacitado) {
        this.costoDeBoletoDeAdultoMayorDiscapacitado = costoDeBoletoDeAdultoMayorDiscapacitado;
    }

    public boolean isAptitudParaMenores() {
        return aptitudParaMenores;
    }

    public void setAptitudParaMenores(boolean aptitudParaMenores) {
        this.aptitudParaMenores = aptitudParaMenores;
    }

    public boolean isAptitudParaAdolescentes() {
        return aptitudParaAdolescentes;
    }

    public void setAptitudParaAdolescentes(boolean aptitudParaAdolescentes) {
        this.aptitudParaAdolescentes = aptitudParaAdolescentes;
    }

    public boolean[][] getSalaDondeSeProyectara() {
        return salaDondeSeProyectara;
    }

    public void setSalaDondeSeProyectara(boolean[][] salaDondeSeProyectara) {
        this.salaDondeSeProyectara = salaDondeSeProyectara;
    }

    // Constructor
    public Funcion(Pelicula peliculaAserProyectada, Hora horaInicio, Double costoGeneral, Double porcentajeAdulto, Double porcentajeNino, Double porcentajeDiscapacitado, Double porcentajeAdultoMayor, String dia, int idDeFuncion) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos, exceptionCostoDeFuncionInvalido {
        if (costoGeneral <= 30 && costoGeneral >= 1) {
            if (peliculaAserProyectada.getCategoriaDePelicula().equals("AA") || peliculaAserProyectada.getCategoriaDePelicula().equals("A")) {
                this.aptitudParaMenores = true;
            }
            if (peliculaAserProyectada.getCategoriaDePelicula().equals("AA") || peliculaAserProyectada.getCategoriaDePelicula().equals("A") || peliculaAserProyectada.getCategoriaDePelicula().equals("B") || peliculaAserProyectada.getCategoriaDePelicula().equals("B15")) {
                this.aptitudParaAdolescentes = true;
            }
            this.peliculaAserProyectada = peliculaAserProyectada;
            this.horaInicio = horaInicio;
            this.horaFin = Hora.sumarHoras(horaInicio, peliculaAserProyectada.getDuracion());
            this.costoGeneral = costoGeneral;
            this.salaDondeSeProyectara = new boolean[6][10];
            this.dia = dia;
            this.idDeFuncion = idDeFuncion;
            this.seccionDelDia = horaInicio.comprobarSeccionDeDia();
            this.costoDeBoletoDeAdulto = this.costoGeneral - (this.costoGeneral * (porcentajeAdulto / 100));
            this.costoDeBoletoDeAdultoDiscapacitado = this.costoDeBoletoDeAdulto - (this.costoGeneral * (porcentajeDiscapacitado / 100));
            this.costoDeBoletoDeAdultoMayor = this.costoGeneral - (this.costoGeneral * (porcentajeAdultoMayor / 100));
            this.costoDeBoletoDeAdultoMayorDiscapacitado = this.costoDeBoletoDeAdultoMayor - (this.costoGeneral * (porcentajeDiscapacitado / 100));
            if (aptitudParaMenores) {
                this.costoDeBoletoDeNino = this.costoGeneral - (this.costoGeneral * (porcentajeNino / 100));
                this.costoDeBoletoDeNinoDiscapacitado = this.costoDeBoletoDeNino - (this.costoGeneral * (porcentajeDiscapacitado / 100));
            } else {
                this.costoDeBoletoDeNino = null;
                this.costoDeBoletoDeNinoDiscapacitado = null;
            }
        } else {
            throw new exceptionCostoDeFuncionInvalido();
        }
    }

    // Other methods
    public void representarEnTabla(DefaultTableModel tabla) {
        tabla.addRow(new Object[]{
            this.peliculaAserProyectada.getNombre(),
            this.dia,
            this.horaInicio.toString(),
            this.seccionDelDia,
            this.costoDeBoletoDeAdulto,
            this.costoDeBoletoDeAdultoDiscapacitado,
            this.costoDeBoletoDeAdultoMayor,
            this.costoDeBoletoDeAdultoMayorDiscapacitado,
            this.costoDeBoletoDeNino,
            this.costoDeBoletoDeNinoDiscapacitado
        });
    }

    private void limpiarTabla(DefaultTableModel tabla) {
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
        for (int i = 0; i < tabla.getRowCount(); i++) {
            tabla.removeRow(i);
        }
    }

    public void representarCostoDeBoletosEnTabla(DefaultTableModel tabla) {
        limpiarTabla(tabla);
        tabla.addRow(new Object[]{
            this.costoDeBoletoDeAdulto,
            this.costoDeBoletoDeAdultoDiscapacitado,
            this.costoDeBoletoDeAdultoMayor,
            this.costoDeBoletoDeAdultoMayorDiscapacitado,
            this.costoDeBoletoDeNino,
            this.costoDeBoletoDeNinoDiscapacitado
        });
    }

    public void representarEnPDF(String ruta) {
        new GeneradorPDF().generarPDF(
                "Comprobante de creación de función",
                "Id. de creación / Id. de función: " + this.idDeFuncion,
                "Película a ser proyectada: " + peliculaAserProyectada.getNombre() + "\n"
                + "Sinópsis de la película: " + peliculaAserProyectada.getSinopsis() + "\n"
                + "Duración de la pelicula: " + peliculaAserProyectada.getDuracion().toString() + "\n"
                + "Dimensión de la película: " + peliculaAserProyectada.getDimension() + "\n"
                + "Categoría de la película: " + peliculaAserProyectada.getCategoriaDePelicula() + "\n"
                + "Clasificación de la película: " + peliculaAserProyectada.getClasificacion() + "\n"
                + "Día de la función: " + this.getDia() + "\n"
                + "Hora de inicio de la función: " + this.getHoraInicio().toString() + "\n"
                + "Sección del día de la función: " + this.getSeccionDelDia() + "\n"
                + "Costo del boleto de adulto: " + this.costoDeBoletoDeAdulto + "\n"
                + "Costo del boleto de adulto con discapacidad: " + this.costoDeBoletoDeAdultoDiscapacitado + "\n"
                + "Costo del boleto de adulto mayor: " + this.costoDeBoletoDeAdultoMayor + "\n"
                + "Costo del boleto de adulto mayor con discapacidad: " + this.costoDeBoletoDeAdultoMayorDiscapacitado + "\n"
                + "Costo del boleto de niño: " + this.costoDeBoletoDeNino + "\n"
                + "Costo del boleto de niño con discapacidad: " + this.costoDeBoletoDeNinoDiscapacitado,
                "Comprobante generado el: " + new Fecha().representar(),
                getClass().getResource("/medios/imagenCreacionDeFuncion.jpg").toString(),
                ruta + ".pdf"
        );
    }

    public int calcularBoletosDisponibles() {
        int r;
        r = 0;
        for (int i = 0; i < salaDondeSeProyectara.length; i++) {
            for (int j = 0; j < salaDondeSeProyectara[i].length; j++) {
                if (!salaDondeSeProyectara[i][j]) {
                    r++;
                }
            }
        }
        return r;
    }

}
