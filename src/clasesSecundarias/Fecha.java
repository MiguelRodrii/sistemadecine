/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesSecundarias;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Usuario
 */
public class Fecha implements Serializable {

    // Atributos
    private int dia;
    private int mes;
    private int año;

    //Contructor
    public Fecha(int dia, int mes, int año) {
        this.dia = dia;
        this.mes = mes;
        this.año = año;
    }

    public Fecha() {
        Calendar c;
        c = new GregorianCalendar();
        this.dia = c.get(Calendar.DAY_OF_MONTH);
        this.mes = c.get(Calendar.MONTH) + 1;
        this.año = c.get(Calendar.YEAR);
    }

    // Gett y set
    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    //Bisiesto
    public boolean comprobarBisiesto() {
        boolean respuesta;
        if (((año % 4 == 0) && (año % 100 != 0)) || (año % 400 == 0)) {
            respuesta = true;
        } else {
            respuesta = false;
        }

        return respuesta;
    }

    // Impar
    private boolean parImpar() {
        boolean respuesta;
        respuesta = true;
        if (mes % 2 == 0) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        return respuesta;
    }

    //Cuantos dias tiene el mes
    public int numeroDeDiasMes() {
        if (mes <= 0 || mes >= 13) {
            return 0;
        } else {
            int respuesta;
            respuesta = 0;
            if (mes <= 7) {
                if (mes == 2 && comprobarBisiesto() == true) {
                    respuesta = 29;
                } else if (mes == 2 && comprobarBisiesto() == false) {
                    respuesta = 28;
                } else if (parImpar() == false) {
                    respuesta = 31;
                } else {
                    respuesta = 30;
                }
            } else {
                if (mes == 2 && comprobarBisiesto() == true) {
                    respuesta = 29;
                } else if (mes == 2 && comprobarBisiesto() == false) {
                    respuesta = 28;
                } else if (parImpar() == false) {
                    respuesta = 30;
                } else {
                    respuesta = 31;
                }
            }
            return respuesta;
        }
    }

    public boolean validarFecha() {
        return numeroDeDiasMes() != 0 && this.dia <= numeroDeDiasMes() && this.año >= 1;
    }

    public void incrementarUnDia() {
        int numerodedias;
        numerodedias = numeroDeDiasMes() + 1;
        dia++;
        if (dia >= numerodedias) {
            mes++;
            dia = 1;
        }
        if (mes >= 12) {
            año++;
            mes = 1;
        }
    }

    public void incrementarNDias(int n) {
        for (int i = 0; i < n; i++) {
            incrementarUnDia();
        }
    }

    public Fecha comparar(Fecha f) {
        if (validarFecha()) {
            Fecha respuesta;
            respuesta = new Fecha();
            if (f.getDia() < this.dia) {
                f.setDia(f.getDia() + f.numeroDeDiasMes());
                f.setMes(f.getMes() - 1);
                respuesta.setDia(f.getDia() - this.dia);
            } else {
                respuesta.setDia(f.getDia() - this.dia);
            }
            if (f.getMes() < this.mes) {
                f.setMes(f.getMes() + 12);
                f.setAño(f.getAño() - 1);
                respuesta.setMes(f.getMes() - this.mes);
            } else {
                respuesta.setMes(f.getMes() - this.mes);
            }
            respuesta.setAño(f.getAño() - this.año);
            return respuesta;
        } else {
            return null;
        }
    }

    public Fecha edad() {
        return comparar(new Fecha());
    }

    //representar
    public String representar() {
        return dia + "-" + mes + "-" + año;
    }

    public boolean equals(Fecha fecha) {
        Fecha aux;
        aux = comparar(fecha);
        return aux.getDia() == 0 && aux.getMes() == 0 && aux.getAño() == 0;
    }
    
    public boolean comprobarSiLaFechaEsHoy() {
        return equals(new Fecha());
    }
    
    public boolean comprobarFechaFutura() {
        return edad().getAño() <= -1;
    }

}
