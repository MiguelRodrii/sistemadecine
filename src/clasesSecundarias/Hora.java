/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesSecundarias;

/**
 *
 * @author Usuario
 */
import exceptions.exceptionHoraIncorrecta;
import exceptions.exceptionMinutosIncorrectos;
import exceptions.exceptionSegundosIncorrectos;
import java.io.Serializable;

public class Hora implements Serializable {

    // Atributos
    private int horas;
    private int minutos;
    private int segundos;

    // Constructor
    public Hora(int horas, int minutos, int segundos) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos, exceptionSegundosIncorrectos {
        if (comprobarHoraCorrecta(horas)) {
            this.horas = horas;
            if (comprobarMinutosCorrectos(minutos)) {
                this.minutos = minutos;
                if (comprobarSeguncosCorrectos(segundos)) {
                    this.segundos = segundos;
                } else {
                    throw new exceptionSegundosIncorrectos();
                }
            } else {
                throw new exceptionMinutosIncorrectos();
            }
        } else {
            throw new exceptionHoraIncorrecta();
        }
    }

    public Hora(int horas, int minutos) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos {
        if (comprobarHoraCorrecta(horas)) {
            this.horas = horas;
            if (comprobarMinutosCorrectos(minutos)) {
                this.minutos = minutos;
                this.segundos = 0;
            } else {
                throw new exceptionMinutosIncorrectos();
            }
        } else {
            throw new exceptionHoraIncorrecta();
        }
    }

    // Getter and setter methods
    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    // Methods
    private boolean comprobarHoraCorrecta(int horas) {
        return horas >= 0 && horas <= 23;
    }

    private boolean comprobarMinutosCorrectos(int minutos) {
        return minutos >= 0 && horas <= 59;
    }

    private boolean comprobarSeguncosCorrectos(int segundos) {
        return comprobarHoraCorrecta(segundos);
    }

    @Override
    public String toString() {
        return this.getHoras() + ":" + this.getMinutos();
    }

    public String comprobarSeccionDeDia() {
        if (this.getHoras() <= 12) {
            return "Matutino";
        } else {
            return "Vespertino";
        }
    }

    // Suma un minuto a la hora "aux"
    public static Hora incrementarUnMinuto(Hora aux) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos {
        Hora auxHora;
        auxHora = new Hora(aux.getHoras(), aux.getMinutos());
        if (auxHora.getMinutos() + 1 >= 60) {
            auxHora.setHoras(auxHora.getHoras() + 1);
            auxHora.setMinutos(0);
        } else {
            auxHora.setMinutos(auxHora.getMinutos() + 1);
        }
        return auxHora;
    }
    
    public int transformarAMinutos() {
        return this.getHoras()*60 + this.getMinutos();
    }

    public static Hora sumarHoras(Hora aux, Hora aux2) throws exceptionHoraIncorrecta, exceptionMinutosIncorrectos {
        int minutos1;
        minutos1 = aux2.transformarAMinutos();
        for (int i = 0; i < minutos1; i++) {
            aux = incrementarUnMinuto(aux);
        }
        return aux;
    }
    
    // Este método comparara las horas a y b, devolvera un numero positivo si a es mayor, negativo si es menor
    // y 0 si es igual;
    public static int comparar(Hora a, Hora b) {
        int auxa, auxb;
        auxa = a.transformarAMinutos();
        auxb = b.transformarAMinutos();
        return auxa - auxb;
    }
}
