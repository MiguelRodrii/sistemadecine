/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesSecundarias;

import java.util.Calendar;

/**
 *
 * @author Usuario
 */
public class Tiempo {

    // Atributos
    private int año;
    private int mes;
    private int dia;
    private int hora;
    private int minuto;
    private int segundo;

    // Constructor
    public Tiempo(int año, int mes, int dia, int hora, int minuto, int segundo) {
        this.año = año;
        this.mes = mes;
        this.dia = dia;
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    public Tiempo() {
        Calendar ahoraCal = Calendar.getInstance();
        this.año = ahoraCal.get(1);
        this.mes = (ahoraCal.get(2) + 1);
        this.dia = ahoraCal.get(5);
        this.hora = ahoraCal.get(10);
        this.minuto = ahoraCal.get(12);
        this.segundo = ahoraCal.get(13);
    }
    
    // Getter and setter methods
    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }
    
    // Other methods
    public void añadirUnSegundo() {
        
    }
}
