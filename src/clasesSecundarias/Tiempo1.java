/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesSecundarias;

import java.util.Calendar;

/**
 *
 * @author Usuario
 */
public class Tiempo1 {

    // Atributos
    private int dia;
    private int hora;
    private int minuto;

    // Constructor
    public Tiempo1(int dia, int hora, int minuto) {
        this.dia = dia;
        this.hora = hora;
        this.minuto = minuto;
    }

    public Tiempo1() {
        Calendar ahoraCal = Calendar.getInstance();
        this.dia = ahoraCal.get(5);
        this.hora = ahoraCal.get(10);
        this.minuto = ahoraCal.get(12);
    }
    
    // Getter and setter methods
    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }
    
    // Other methods
    public void añadirUnSegundo() {
        
    }
}
